import React from 'react';
import SearchBar from './SearchBar';
import axios from 'axios';
import UserGitHubProfileList from './UserGitHubProfileList';
import gitHubApiSetting from '../api/gitHubApiSetting';
import PackageStats from './PackageStats';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {repos: [], page_num: 0, package_stats: {keys: new Set() , packages: {}}};
        this.search_type = {new_search: 0, prev: -1, next: 1};
        // this.package_stats = {keys: new Set() , packages: {}};
    }

    onSearchBarSubmit = async (query, search_type) => {
        gitHubApiSetting.get('search/repositories', {
            params: {
                q: query,
                sort: 'forks',
                order: 'desc',
                per_page: 10,
                page: this.state.page_num + search_type
            }
        })
        .then((response) => {
            // console.log(response);
            if (search_type === this.search_type.new_search){
                this.setState({page_num: 1});
            }
            if (search_type === this.search_type.prev && this.state.page_num > 1){
                this.setState({page_num: this.state.page_num - 1});
            }
            if (search_type === this.search_type.next){
                this.setState({page_num: this.state.page_num + 1});
            }

            // console.log(this.state.page_num);

            if (response.data.items.length !== 0) {
                this.setState({repos: response.data.items});
            } else {
                this.setState({repos: []});
                // setTimeout is used to put alert in EventLoop along with this.setState
                setTimeout(function() { alert("No respositories were found."); }, 0);
            }
        })
        .catch((response) => {
            console.log(response);
        });
    }

    onImportButtonClick = async (id, url) => {
        // fetch package.json only if it was not fecthed before.
        if (!this.state.package_stats.keys.has(id)) {
            axios({
                method: 'get',
                url: url,
            })
            .then((response) => {
                try {
                    if (response.data.encoding === 'base64') {
                        const content = atob(response.data.content)
                        const devDependencies = JSON.parse(content).devDependencies;
                        const packagesKeysList = Object.keys(devDependencies);
                        // package_stats object is assigned to tempPackages as json object does not have methods.
                        let tempPackages = Object.assign({}, this.state.package_stats);
                        packagesKeysList.forEach((value) => {
                            if (value in tempPackages.packages) {
                                tempPackages.packages[value] = tempPackages.packages[value] + 1;
                            } else {
                                tempPackages.packages[value] = 1;
                            }
                        });
                        this.setState({package_stats: tempPackages});
                        this.setState({keys: this.state.package_stats.keys.add(id)});
                    }
                }
                catch(error) {
                    console.log('Error in processing base64 blob content');
                    console.error(error);
                }
            })
            .catch((response) => {
                this.setState({keys: this.state.package_stats.keys.add(id)});
                window.alert('Repository does not contain package.json file');
            });
        }
        console.log(this.state.package_stats);
    }

    render() {
        return (
            <div className="container-fluid">
                <SearchBar onSearchSubmit={this.onSearchBarSubmit} page_num={this.state.page_num} search_type={this.search_type}></SearchBar>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-8 border-right border-top" >
                            <h3><u>Repo Search Results</u></h3>
                            <UserGitHubProfileList repos={this.state.repos} onImportButtonClick={this.onImportButtonClick} repo_keys={this.state.package_stats.keys}></UserGitHubProfileList>
                        </div>
                        <div className="col-4 border-top">
                            <h3><u>Package Statistics</u></h3>
                            <PackageStats packages={this.state.package_stats.packages}></PackageStats>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default App;