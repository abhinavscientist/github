import React from 'react';

class SearchBar extends React.Component {

    constructor(props) {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.state = {query: ''};
    }

    search(search_type) {
        if (this.state.query === '') {
            alert('Please enter your query for search');
        } else {
            this.props.onSearchSubmit(this.state.query, search_type);
        }
    }

    onFormSubmit(event) {
        event.preventDefault();
        this.search(this.props.search_type.new_search);
    }

    onClickPrev = async (event) => {
        this.search(this.props.search_type.prev);
    }
    
    onClickNext = async (event) => {
        this.search(this.props.search_type.next);
    }

    render() {
        // const pageNumDisplay = (this.props.page_num === 0) ? "none":"block";
        const nextButtonDisplay = (this.props.page_num > 0) ? false: true;
        const prevButtonDisplay = (this.props.page_num > 1) ? false: true; 
        return (
            <div className="row">
                <div className="col-4 text-right mst-1">
                    <form onSubmit={this.onFormSubmit}>
                        <label className="mr-2"><b>Input your query: </b></label>
                        <input text={this.state.query} onChange={(e) => { this.setState({ query: e.target.value }) }}></input>
                    </form>
                </div>
                <div className="col-2">
                    <button className="btn btn-block btn-primary" disabled={prevButtonDisplay} onClick={this.onClickPrev}>prev</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-block btn-primary" disabled={nextButtonDisplay} onClick={this.onClickNext}>next</button>
                </div>
                <div className="col-2">
                    <h6 className="d-inline-flex p-2 bd-highlight">{`Page: ${this.props.page_num}`}</h6>
                </div>
            </div>
            
        );
    }

}

export default SearchBar;