import React from 'react';

class PackageStats extends React.Component{
    
    sortPackages = () => {
        let sortable = [];
        for (var key in this.props.packages){
            sortable.push([key, this.props.packages[key]]);
        }
        sortable.sort(function(first, second){
            return second[1] - first[1]
        });
        return sortable;
    }
    
    render(){
        const sortedTopTen = this.sortPackages().slice(0,10);
        var rows = [];
        // Listing all rows
        // Object.keys(this.props.packages).forEach((key) => {
        //     rows.push(<li key={key}>{`${key}: ${this.props.packages[key]}`}</li>)
        // });
        sortedTopTen.forEach((element) => {
            rows.push(<li key={element[0]}>{`${element[0]}: ${element[1]}`}</li>)
        });
        
        
        return (
                <ul>
                    {rows}
                </ul>
        );
    }
}

export default PackageStats;