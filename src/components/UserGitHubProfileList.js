import React from 'react';
import UserGitHubProfile from './UserGitHubProfile';

const UserGitHubProfileList = (props) => {
    const repos = props.repos.map((current_repo) => {
        const import_object = {};
        try{
            import_object['package_url'] = current_repo.contents_url.split('/').slice(0,-1).concat('package.json').join('/');
            import_object['id'] = current_repo.id;
        }
        catch(error){
            console.log('Error in building url for package.json ');
            console.error(error);
        }
        return <UserGitHubProfile key={current_repo.id} item={current_repo} import_object={import_object} onImportButtonClick={props.onImportButtonClick} repo_keys={props.repo_keys}></UserGitHubProfile>
    });
    return <div className="repo-list">{repos}</div>
};

export default UserGitHubProfileList;