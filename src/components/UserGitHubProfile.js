import React from 'react';

class UserGitHubProfile extends React.Component {

    onClick = (event) => {
        this.props.onImportButtonClick(this.props.import_object.id, this.props.import_object.package_url);
    }

    render() {
        let isButtonDisabled = this.props.repo_keys.has(this.props.import_object.id)
        return (
            <div className="ui items">
                <div className="item">
                    <div className="image">
                        <img alt="" src={this.props.item.owner.avatar_url} />
                    </div>
                    <div className="content">
                        <a className="header" href="/">{`Owner: ${this.props.item.name}`}</a>
                        <div className="meta">
                            <span>{this.props.item.description}</span>
                        </div>
                        <div className="description">
                            <p></p>
                        </div>
                        <div className="extra">
                            {`Fork Count: ${this.props.item.forks_count}`}
                        </div>
                        <div>
                            <button disabled={isButtonDisabled} onClick={this.onClick} value={this.props.import_object}>Import</button>
                        </div>
                    </div>
                </div>
            </div>
                );
                }
            }
export default UserGitHubProfile;